1. This collection contains all record IDs, i.e. accession numbers, used in our work.

2. The folder name indicates the organism name. In total we used 5 organisms.

3. Each folder contains 2 files: distinct_pairs and duplicate_pairs, the former
is IDs for all distinct pairs, whereas the latter is for duplicate pairs. Since
we are doing duplicate record detection (which uses pairwise comparison), each 
line of the file contains the accession numbers of the records in the pair.

4. Full records can be downloaded using NCBI search engine, or in bacth using 
NCBI E-utilities (http://www.ncbi.nlm.nih.gov/books/NBK25500/).